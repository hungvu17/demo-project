/*
Author: Minh-Hung VU

Last update: 00:22, 03/24/2019

Summary: 
- Integrating to master

Ongoing: None

Stuck: None

Checklist:
[x] image size
[x] image color
[x] image dimension
[x] image content
[x] display result
*/


import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FeatureA from './components/featureA';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <div>Develop Feature A: big, red</div>
        </header>
      </div>
    );
  }
}

export default App;
