# Demo Project

## Requirement
**Dependencies**

Node version 10.0

React version 16.8.5

Native-base version 2.12.1

**Development Plugin**

Eslint version 0.54.2

Prettier version 0.2.1


## Environment
### Device
1. MacOS
- Version 10.14.04
- CPU 2.3GHz Intel Core i5
- Ram 8GB
- SSD 256GB
2. Window
- Window 10 Pro
- CPU 2,3GHz Intel Core i7
- Ram 4GB
- HDD 1TB
### Browser
1. Chrome 73.0
2. Safari 12.0.3

## Build script
1. Clone project/Pull branch
2. Install dependencies
```
npm install
```
3. Build script
- Localhost:
```
npm start
```
- Deployment
```
npm run build
```
## Version
v0.3.0 : demo with image content
v0.2.0 : demo with left right features

v0.1.0 : demo with cataract
